import selenium.webdriver
import requests
from collections import namedtuple

class TumblrArchiverState:
	browser = None
	blog_container = None
	bottom_anchor = None
	requests_session = None
	skip_reblogs = False

state = TumblrArchiverState()
state.browser = selenium.webdriver.Firefox()
state.requests_session = requests.Session()
#!/bin/ipython --pdb
#from IPython import embed as repl

import io
import os
from time import sleep
from selenium.webdriver.support.ui import Select
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException, ElementClickInterceptedException
from selenium.webdriver.common.by import By
from bs4 import BeautifulSoup as bs
from urllib.parse import urlparse
from private import state
import json.encoder
import requests
import os.path
from hashlib import md5 as hash
import fileinput

format_version = 1

delay = 10
new_posts_delay = 5 # better be sure
following_page_length = 25
valid_url_schemes = set(['http', 'https'])
chunk_size = 4096

def mkdirp(*argv, **argkv):
	try:
		os.mkdir(*argv, **argkv)
	except FileExistsError:
		pass

def force_click(element):
	return selenium.webdriver.ActionChains(state.browser).move_to_element(element).click(element).perform()

def by_id(id):
	return state.browser.find_element_by_id(id)

def wait_by_id(id):
	return WebDriverWait(state.browser, delay).until(EC.presence_of_element_located((By.ID, id)))

def wait_by_class(id):
	return WebDriverWait(state.browser, delay).until(EC.presence_of_element_located((By.CLASS_NAME, id)))

def wait_until_stale(el):
	WebDriverWait(state.browser, delay).until(EC.staleness_of(el))

def write_cookies():
	with io.open('cookies.json', 'w') as cookies:
		cookies.write(json.encoder.JSONEncoder().encode(state.browser.get_cookies()))

def copy_cookies_to_requests():
	for cookie in state.browser.get_cookies():
		if 'expiry' in cookie:
			del cookie['expiry']
		if 'httpOnly' in cookie:
			del cookie['httpOnly']
		state.requests_session.cookies.set_cookie(requests.cookies.create_cookie(**cookie))

def login():
	state.browser.get(input("email"))
	wait_by_id("signup_determine_email")
	wait_by_id("signup_determine_email").send_keys("raingloom42@gmail.com")
	WebDriverWait(state.browser, delay).until(EC.visibility_of_element_located((By.ID, "signup_forms_submit"))).click()
	WebDriverWait(state.browser, delay).until(EC.visibility_of_element_located((By.CLASS_NAME, "magiclink_password_container"))).click()
	wait_by_id("signup_password").send_keys(input("password"))
	e=WebDriverWait(state.browser, delay).until(EC.visibility_of_element_located((By.ID, "signup_forms_submit")))
	e.click()
	wait_until_stale(e)
	copy_cookies_to_requests()

def get_following(nth):
	state.browser.get("https://tumblr.com/following/" + str(nth))
	return list(map(lambda e:e.get_attribute('href'), state.browser.find_elements_by_class_name("name-link")))

def stream_followings():
	i=0
	while True:
		l=get_following(i)
		i+=len(l)
		if len(l) < following_page_length:
			return l
		else:
			yield l

def loaded_blog_name():
	return state.browser.current_url.split('/')[-1]

def get_blog_container():
	name = loaded_blog_name()
	containers = state.browser.find_elements_by_class_name("posts")
	for container in containers:
		if all(map(lambda s:s==name, map(lambda e:e.get_attribute("data-tumblelog-name"), container.find_elements_by_class_name("post")))):
			return container

def make_blog_dir():
	d = 'data/' + loaded_blog_name()
	mkdirp(d)
	mkdirp(d + '/posts')
	with io.open(d + '/format_version', 'w') as file:
		file.write(str(format_version))
	return d

def load_blog(name):		
	state.browser.get("https://tumblr.com/dashboard/blog/" + name)
	wait_by_class("indash_blog") # wait for sidebar thingy to load
	state.blog_container = get_blog_container()
	state.bottom_anchor = state.browser.execute_script("let e=document.createElement('div'); e.setAttribute('height','1000px'); e.id='_tumblr_archiver_bottom_anchor'; arguments[0].parentNode.appendChild(e); return e;", state.browser.find_element_by_class_name("peepr-big-loader"))
	state.top_anchor = state.browser.execute_script("let p = arguments[0].parentNode; let e=document.createElement('div'); e.setAttribute('height','1000px'); e.id='_tumblr_archiver_top_anchor'; arguments[0].parentNode.insertBefore(e, p.firstChild); return e;", state.browser.find_element_by_class_name("indash_blog"))

def scroll_into_view(element):
	state.browser.execute_script('arguments[0].scrollIntoView(true);', element)

def get_loaded_posts():
	return state.blog_container.find_elements_by_class_name('post')

def delete_element(element):
	state.browser.execute_script('arguments[0].parentNode.removeChild(arguments[0]);', element)

def load_new_posts():
	scroll_into_view(state.top_anchor)
	scroll_into_view(state.bottom_anchor)

def explosive_stream_posts():
	while True:
		d = new_posts_delay
		posts = get_loaded_posts()
		while d and not posts:
			load_new_posts()
			sleep(1)
			d-=1
			posts = get_loaded_posts()
		if not posts:
			return
		if len(posts) > 1:
			for post in posts[0:-2]:
				yield post
		yield posts[-1]
		load_new_posts()

def stream_and_consume_posts():
	for post in explosive_stream_posts():
		yield post
		delete_element(post)

def get_post_id(post):
	return post.get_attribute('data-post-id')

def inner_html(element):
	return state.browser.execute_script('return arguments[0].innerHTML', element)

def get_header():
	return wait_by_class('indash_blog').find_element_by_class_name('header')

def make_post_dir(post):
	d = make_blog_dir() + '/posts/' + get_post_id(post)
	mkdirp(d)
	mkdirp(d + '/media')
	mkdirp(d + '/other')
	return d

def save_element_html_in(post, d):
	with io.open(d + '/post.html', 'w') as file:
		file.write(inner_html(post))

def save_post(post):
	save_post_html(post)
	save_post_data(post)

def save_post_html(post):
	save_element_html_in(post, make_post_dir(post))

def get_url_name(url):
	return str(hash(bytes(url.geturl(),'utf8')).hexdigest())+'-'+(url.path.split('/')[-1] or '-unknown')

def save_url_in(url, d):
	f = d + '/' + get_url_name(url)
	if os.path.isfile(f):
		return
	with io.open(f, 'wb') as file:
		print('saving', url, 'to', f)
		for chunk in state.requests_session.get(url.geturl()).iter_content(chunk_size = chunk_size):
			print('.', end='')
			file.write(chunk)
		print()

def save_element_data_in(post, d):
	media = d + '/media'
	other = d + '/other'
	for url in urls_in_post(post):
		if is_media_url(url):
			save_url_in(url, media)
		elif is_worthy_url(url):
			save_url_in(url, other)
		else:
			print('skipping:',url)

def save_post_data(post):
	save_element_data_in(post, make_post_dir(post))

def urls_in_post(post):
	for node in bs(inner_html(post))():
		for vals in node.attrs.values():
			if type(vals) != list:
				vals = [vals]
			for val in vals:
				url=urlparse(val)
				if url.scheme and url.scheme in valid_url_schemes:
					yield url

def is_root_url(url):
	return url.path == '/'

def is_image_url(url):
	return url.path.startswith('/image')

def is_tmblr_url(url):
	return url.netloc == 'tmblr.co'

def is_user_url(url):
	loc=url.netloc.split('.')
	return is_root_url(url) and len(loc)==2 and loc[0] != 'assets'

def is_video_url(url):
	return url.path.startswith('/video_file/')

def is_media_url(url):
	loc=url.netloc.split('.')
	return len(loc)==4 and loc[1]=='media'

def is_worthy_url(url):
	return url.netloc.endswith('tumblr.com') and not (is_root_url(url) or is_user_url(url) or is_tmblr_url(url) or is_image_url(url))

def save_header():
	h = get_header()
	d = make_blog_dir() + '/header'
	mkdirp(d)
	mkdirp(d + '/media')
	mkdirp(d + '/other')
	save_element_html_in(h, d)
	save_element_data_in(h, d)
	return d

def is_reblog(post):
	return not not post.find_elements_by_class_name('reblog-list')

def save_loaded_blog():
	make_blog_dir()
	save_header()
	for post in stream_and_consume_posts():
		if not (state.skip_reblogs and is_reblog(post)):
			save_post(post)

def save_blog(name):
	load_blog(name)
	save_loaded_blog()

def process_line(line):
	line = line.split(' ')
	blog = line[0]
	options = line[1:]
	state.skip_reblogs = not 'get_reblogs' in options
	load_blog(blog)
	if loaded_blog_name().strip() == blog.strip():
		save_blog(blog)
	else:
		print('warning: loading blog' + blog + 'failed, did you type the name correctly?')

def process_blogs_from_file(file):
	for line in file.readlines():
		process_line(line)
